import "./App.css";
import Main_Layout from "./Main_Layout/Main_Layout";

function App() {
  return (
    <div className="App">
      <Main_Layout />
    </div>
  );
}

export default App;
