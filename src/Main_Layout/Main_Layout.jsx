import React, { Component } from "react";
import Banner from "./Banner";
import Header from "./Header";
import List from "./List";

export default class Main_Layout extends Component {
  render() {
    return (
      <div>
        <Header />
        <Banner />
        <List />
      </div>
    );
  }
}
